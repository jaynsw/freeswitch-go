package freeswitch

import (
	"fmt"
	"github.com/yuin/gopher-lua"
	"github.com/yuin/gopher-lua/parse"
	"os"
	"path/filepath"
	"time"
)

const eventsBuffer = 16

type LuaProcessor struct {
	factory     *LuaProcessorFactory
	event       chan *Message
	Close       bool
	engine      Engine
	initMessage *Message
	properties  map[string]string
	lua         *lua.LState
}

type LuaProcessorFactory struct {
	Properties  map[string]string
	luaModified time.Time
	luaPBX      string
	proto       *lua.FunctionProto
}

func postEventToLua(h *LuaProcessor, callback string, message *Message) error {
	l := h.lua
	engine := h.engine
	lv := l.GetGlobal(callback)
	if _, ok := lv.(*lua.LFunction); ok {
		t := l.NewTable()
		header := message.Header
		for k, v := range header {
			t.RawSet(lua.LString(k), lua.LString(v))
		}

		if message.Body != "" {
			t.RawSet(lua.LString("__message_body"), lua.LString(message.Body))
		}
		l.CallByParam(lua.P{Fn: lv, NRet: 0, Protect: false}, t)
		return nil
	} else {
		//return fmt.Errorf("No Lua callback %s methond found!", callback)
		engine.Log("LuaProcessor post envent:" + callback)
		return nil
	}

}

func (h *LuaProcessor) init() {
	h.event = make(chan *Message, eventsBuffer)
}

func (h *LuaProcessorFactory) compile() (proto *lua.FunctionProto, err error) {
	ff, _ := os.Open(h.luaPBX)

	name := filepath.Base(h.luaPBX)
	chunk, err := parse.Parse(ff, name)
	if err != nil {
		return nil, err
	}
	proto, err = lua.Compile(chunk, name)
	if err != nil {
		return proto, err
	}
	h.proto = proto
	return proto, err
}

func newApiError(code lua.ApiErrorType, message string, object lua.LValue) *lua.ApiError {
	if len(message) > 0 {
		object = lua.LString(message)
	}
	return &lua.ApiError{code, object}
}

func (h *LuaProcessor) Start() error {

	luaFile := h.factory.luaPBX
	ff, err := os.Open(luaFile)
	if err != nil {
		return err
	}
	defer ff.Close()

	fi, _ := ff.Stat()
	tm := fi.ModTime()

	if tm.After(h.factory.luaModified) {
		h.factory.luaModified = tm
		h.engine.Log("compile %s", luaFile)
		_, err := h.factory.compile()
		if err != nil {
			return err
		}
	}

	ls := lua.NewState()

	freeswitchOpen(ls)
	uuidOpen(ls)
	httpOpen(ls)

	ctx := &FreeSwitchContext{
		Engine:    h.engine,
		Processor: h,
	}

	regtable := ls.Get(lua.RegistryIndex)
	ud := ls.NewUserData()
	ud.Value = ctx

	ls.SetField(regtable, lFreeSwitchContextClass, ud)

	h.lua = ls

	h.engine.Log("run lua file:" + luaFile)
	//h.lua.DoFile(luaFile)
	fn := &lua.LFunction{
		IsG: false,
		Env: ls.Env,

		Proto:     h.factory.proto,
		GFunction: nil,
		Upvalues:  make([]*lua.Upvalue, 0),
	}
	ls.Push(fn)
	err = ls.PCall(0, lua.MultRet, nil)
	if err != nil {
		return err
	}
	engine := h.engine
	engine.Log("LuaProcessor started")

	go func() {
		engine.Log("event loop init")

		h.engine.Log("Event OnInit")
		postEventToLua(h, "OnInit", h.initMessage)

		for {
			if h.Close {
				break
			}
			select {

			case msg := <-h.event:
				name := msg.Header["Event-Name"]
				engine.Log("channel received event named %s", name)
				postEventToLua(h, "OnEvent", msg)

			}
		}
	}()

	return err
}

func (h *LuaProcessor) GetMessageChannel() chan<- *Message {
	return h.event
}

func NewFactory(props map[string]string) *LuaProcessorFactory {

	cwd, err := os.Getwd()
	if err != nil {
		return nil
	}

	path := os.Getenv("LUA_PATH")
	if len(path) == 0 {
		path = cwd + "/?.lua"
	} else {
		path = path + ";" + cwd + "/?.lua"
	}
	os.Setenv("LUA_PATH", path)

	luaFile, ok := props["lua"]
	if !ok {
		panic("file pbx.lua not found")
	}
	pbx := cwd + "/" + luaFile

	ff, err := os.Open(pbx)

	if err != nil {
		panic("file pbx.lua can not be open")
	}
	defer ff.Close()
	fi, _ := ff.Stat()

	factory := &LuaProcessorFactory{
		Properties:  props,
		luaPBX:      pbx,
		luaModified: fi.ModTime(),
	}

	_, err = factory.compile()
	if err != nil {
		panic("pbx.lua compile error:" + err.Error())
	}
	fmt.Println("ProcessorFactory started...")
	return factory

}

func (h *LuaProcessorFactory) New(eng Engine, msg *Message) EventProcessor {
	processor := &LuaProcessor{
		factory:     h,
		engine:      eng,
		initMessage: msg,
		properties:  h.Properties,
		event:       make(chan *Message, eventsBuffer),
	}
	processor.init()

	return processor
}
