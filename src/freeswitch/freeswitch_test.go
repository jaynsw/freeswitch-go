// freeswitch_test.go
package freeswitch_test

import (
	. "freeswitch"
	"log"
	"testing"
	"time"
)

type uselessEventHandler struct {
}

func (h *uselessEventHandler) HandleEvent(ch *Channel, ev *Message) error {
	name := ev.Header["Event-Name"]
	log.Printf("channelId:%s,eventName:%s", ch.ChannelId, name)
	return nil
}

func (h *uselessEventHandler) Start() error {
	return nil
}

type uselessEventHandlerFactory struct {
}

func (factory *uselessEventHandlerFactory) New(engine Engine, ch *Channel, msg *Message) EventHandler {
	return new(uselessEventHandler)
}

func TestEngine(t *testing.T) {

	factory := new(uselessEventHandlerFactory)
	engine, err := Dial("localhost:8021", "ClueCon", factory)
	if err != nil {
		log.Printf("Engine Dial error:%v", err)
		return
	}
	for {
		time.Sleep(time.Second)
	}
	engine.Close()

}
