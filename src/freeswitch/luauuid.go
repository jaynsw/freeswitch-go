package freeswitch

import (
	"github.com/yuin/gopher-lua"
	"uuid"
)

func uuidOpen(L *lua.LState) {
	L.RegisterModule("uuid", fsuuidMethods)
}

var fsuuidMethods = map[string]lua.LGFunction{
	"new": newUUID,
}

func newUUID(L *lua.LState) int {
	L.Push(lua.LString(uuid.New()))
	return 1
}
