// channel.go
package freeswitch

import ()

type ChannelState int

const (
	INIT ChannelState = iota
	ACTIVE
	DEAD
)

type Channel struct {
	State         ChannelState
	ChannelId     string
	Engine        Engine
	CallDirection string
	Processor     EventProcessor
	From          string
	To            string
	Attributes    MessageHeader
}

func (ch *Channel) handleEvent(msg *Message) error {
	if ch.Processor != nil {
		chh := ch.Processor.GetMessageChannel()
		chh <- msg

	}
	return nil
}
