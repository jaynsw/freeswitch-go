package freeswitch

import (
	"github.com/yuin/gopher-lua"
	"time"
)

const lFreeSwitchContextClass = "FreeSwitchContext*"

type FreeSwitchContext struct {
	Engine    Engine
	Processor EventProcessor
}

func getContext(L *lua.LState) *FreeSwitchContext {
	regtable := L.Get(lua.RegistryIndex)
	lv := L.GetField(regtable, lFreeSwitchContextClass)
	if ud, ok := lv.(*lua.LUserData); ok {
		if ctx, ok := ud.Value.(*FreeSwitchContext); ok {
			return ctx
		}
	}
	L.ArgError(1, "freeswitchctx expected")
	return nil
}

func freeswitchOpen(L *lua.LState) {
	L.RegisterModule("freeswitch", fsMethods)
}

var fsMethods = map[string]lua.LGFunction{
	"createchannel": createchannel,
	"bgapi":         bgapi,
	"sendmsg":       sendmsg,
	"executeuuid":   executeuuid,
}

func createchannel(L *lua.LState) int {
	channelID := L.CheckString(1)
	ctx := getContext(L)

	ctx.Engine.CreateChannel(channelID, ctx.Processor)
	return 0
}

func bgapi(L *lua.LState) int {
	delay := L.CheckInt64(1)
	cmd := L.CheckString(2)
	ctx := getContext(L)

	ctx.Engine.BGAPI(time.Duration(delay)*time.Millisecond, cmd, nil)
	return 0
}

func sendmsg(L *lua.LState) int {
	delay := L.CheckInt64(1)
	uuid := L.CheckString(2)
	msg := L.CheckTable(3)
	data := L.OptString(4, "")
	ctx := getContext(L)
	ssg := make(map[string]string)
	msg.ForEach(func(k, v lua.LValue) {
		ssg[k.String()] = v.String()
	})

	ok, err := ctx.Engine.SendMSG(time.Duration(delay)*time.Millisecond, uuid, ssg, data)
	if err != nil {
		L.Push(lua.LFalse)
	} else {
		L.Push(lua.LBool(ok))
	}
	return 1
}

func executeuuid(L *lua.LState) int {
	delay := L.CheckInt64(1)
	uuid := L.CheckString(2)
	appName := L.CheckString(3)
	appArg := L.OptString(4, "")
	ctx := getContext(L)

	ok, err := ctx.Engine.ExecuteUUID(time.Duration(delay)*time.Millisecond, uuid, appName, appArg)
	if err != nil {
		L.Push(lua.LFalse)
	} else {
		L.Push(lua.LBool(ok))
	}
	return 1
}
