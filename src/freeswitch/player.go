// player.go
package freeswitch

import (
	"time"
)

type Player struct {
	Engine  Engine
	Channel *Channel
}

func (p *Player) Hangup(delay time.Duration, cause string, callback func(bool)) error {

	return p.uuidCommand(delay, "uuid_kill", cause, callback)
}

func (p *Player) Event(delay time.Duration, subclass string) (bool, error) {
	return p.Engine.ExecuteUUID(delay, p.Channel.ChannelId, "event", "Event-Subclass="+subclass)
}

func (p *Player) Answer(delay time.Duration, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_answer", "", callback)
}

func (p *Player) Break(delay time.Duration, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_break", "all", callback)
}

func (p *Player) SendDTMF(delay time.Duration, dtmf string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_send_dtmf", dtmf, callback)
}

func (p *Player) FileExists(delay time.Duration, path string, callback func(bool)) error {

	return p.uuidCommand(delay, "file_exists", path, callback)
}

func (p *Player) uuidCommand(delay time.Duration, cmd, args string, callback func(bool)) error {
	data := p.Channel.ChannelId
	if args != "" {
		data += " " + args
	}
	return p.Engine.BGAPI(delay, cmd+" "+data, callback)
}

func (p *Player) Bridge(delay time.Duration, channelId string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_bridge", channelId, callback)
}

func (p *Player) Playback(delay time.Duration, path string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_broadcast", "playback::"+path, callback)
}

func (p *Player) RecordStart(delay time.Duration, path string, maxduration string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_record", "start "+path+" "+maxduration, callback)
}

func (p *Player) RecordStop(delay time.Duration, path string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_record", "stop "+path, callback)
}

func (p *Player) RecordMask(delay time.Duration, path string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_record", "mask "+path, callback)
}

func (p *Player) RecordUnMask(delay time.Duration, path string, callback func(bool)) error {
	return p.uuidCommand(delay, "uuid_record", "unmask "+path, callback)
}
