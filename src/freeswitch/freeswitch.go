package freeswitch

import (
	"bufio"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"net/textproto"
	"net/url"
	//	"runtime"
	"strconv"
	"strings"
	"sync"
	"time"
)

const bufferSize = 1024 << 6

type Engine interface {
	CreateChannel(channelId string, processor EventProcessor) *Channel
	BGAPI(delay time.Duration, command string, callback func(bool)) error
	SendMSG(delay time.Duration, uuid string, m MSG, appData string) (bool, error)
	ExecuteUUID(delay time.Duration, uuid, appName, appArg string) (bool, error)
	Close() error
	Log(format string, args ...interface{})
}

type EventHandler interface {
	HandleEvent(msg *Message) error
	Start() error
}

type EventProcessor interface {
	Start() error
	GetMessageChannel() chan<- *Message
}

type EventProcessorFactory interface {
	New(engine Engine, msg *Message) EventProcessor
}

type SimpleEngine struct {
	processorFactory EventProcessorFactory
	jobsLock         sync.Mutex
	writeLock        sync.Mutex
	jobs             map[string]chan *Message
	channels         map[string]*Channel
	chsLock          sync.RWMutex
	conn             net.Conn
	reader           *bufio.Reader
	textreader       *textproto.Reader
	err              chan error
	cmd, api         chan *Message
	close            bool
	ErrorLog         *log.Logger
}

type MessageHeader map[string]string

type Message struct {
	Header MessageHeader
	Body   string
}

type MSG map[string]string

var ErrMissingAuthRequest = errors.New("Missing auth request")
var ErrInvalidPassword = errors.New("Invalid password")
var ErrInvalidCommand = errors.New("Invalid command contains \\r or \\n")
var ErrEventSubscrib = errors.New("Event Subscrib not successful")
var ErrBGAPI = errors.New("BGAPI error")

func Dial(addr, password string, factory EventProcessorFactory) (Engine, error) {

	log.Printf("Dial addr:%s,password:%s", addr, password)

	c, err := net.Dial("tcp", addr)

	if err != nil {
		c.Close()
		return nil, err
	}

	engine := &SimpleEngine{
		processorFactory: factory,
		err:              make(chan error),
		cmd:              make(chan *Message),
		api:              make(chan *Message),
		conn:             c,
		reader:           bufio.NewReaderSize(c, bufferSize),
	}
	engine.textreader = textproto.NewReader(engine.reader)
	engine.channels = make(map[string]*Channel)
	engine.jobs = make(map[string]chan *Message)
	engine.Log("start read first message from server")
	m, err := engine.textreader.ReadMIMEHeader()
	if err != nil {
		return nil, err
	}

	contentType := m.Get("Content-Type")
	engine.Log("message header Content-Type:%s", contentType)

	if "auth/request" != contentType {
		c.Close()
		return nil, ErrMissingAuthRequest
	}
	engine.Log("auth with password:%s", password)
	fmt.Fprintf(c, "auth %s\r\n\r\n", password)
	m, err = engine.textreader.ReadMIMEHeader()
	if err != nil {
		c.Close()
		return nil, err
	}
	replyTxt := m.Get("Reply-Text")
	if !strings.HasPrefix(replyTxt, "+OK") {
		c.Close()
		return nil, ErrInvalidPassword
	}

	engine.Log("start to subscrib all even")
	fmt.Fprintf(c, "event %s\r\n\r\n", "plain ALL")
	m, err = engine.textreader.ReadMIMEHeader()
	if err != nil {
		c.Close()
		return nil, err
	}
	replyTxt = m.Get("Reply-Text")

	if !strings.HasPrefix(replyTxt, "+OK") {
		c.Close()
		return nil, ErrEventSubscrib
	}

	engine.Log("start to readLoop of event")
	go func() {

		for {
			log.Println("loop readOnce")
			if !engine.readOne() {
				log.Println("error:%v", engine.err)
				engine.Close()
				break
			}
			if engine.close {
				break
			}
		}
	}()

	return engine, err
}

func (s *SimpleEngine) Log(format string, args ...interface{}) {
	if s.ErrorLog != nil {
		s.ErrorLog.Printf(format, args...)
	} else {
		log.Printf(format, args...)
	}
}

func (engine *SimpleEngine) readOne() bool {
	hdr, err := engine.textreader.ReadMIMEHeader()
	if err != nil {
		engine.err <- err
		return false
	}

	resp := new(Message)
	resp.Header = make(MessageHeader)
	resp.Body = ""

	engine.Log("---readOne message:%v", hdr)

	contentType := hdr.Get("Content-Type")
	engine.Log("Header Content-Type:%s", contentType)

	if v := hdr.Get("Content-Length"); v != "" {
		length, err := strconv.Atoi(v)
		if err != nil {
			engine.Log("error in reading Content-Length:%v", err)
			engine.err <- err
			return false
		}
		engine.Log("Content-Length:%d", length)
		engine.Log("buffered:%d", engine.reader.Buffered())
		b := make([]byte, length)
		n, err := io.ReadFull(engine.reader, b)
		if err != nil {
			engine.Log("error in reading message body", err)
			engine.err <- err
			return false
		}
		engine.Log("read len= %d", n)

		reader := bufio.NewReader(bytes.NewReader(b))

		textreader := textproto.NewReader(reader)
		hdr, err = textreader.ReadMIMEHeader()
		if err != nil {
			engine.Log("error in reading text/event-plain body:%s", resp.Body)
			engine.err <- err
			return false
		}
		engine.Log("event hdr:%v", hdr)
		if v := hdr.Get("Content-Length"); v != "" {
			length, err := strconv.Atoi(v)
			if err != nil {
				engine.Log("error in reading event Content-Length:%v", err)
				engine.err <- err
				return false
			}
			engine.Log("event Content-Length:%d", length)
			b := make([]byte, length)
			n, err := io.ReadFull(reader, b)
			if err != nil {
				engine.Log("error in reading event body", err)
				engine.err <- err
				return false
			}
			resp.Body = string(b[:n])
			engine.Log("eve nt body:%v", resp.Body)
		}

	}

	switch contentType {
	case "command/reply":
		reply := hdr.Get("Reply-Text")
		if reply[:4] == "-ERR" {
			engine.Log("error in Reply-Text:%s", reply)
			engine.err <- errors.New(reply[5:])
			return true
		}
		if reply[0] == '%' {
			copyHeaders(&hdr, resp, true)
		} else {
			copyHeaders(&hdr, resp, false)
		}
		//engine.Log("command/reply response:%v", resp)
		engine.cmd <- resp

	case "api/response":
		if string(resp.Body[:4]) == "-ERR" {
			engine.Log("error in api/response body:%s", resp.Body)
			engine.err <- errors.New(string(resp.Body[5:]))
			return true
		}
		copyHeaders(&hdr, resp, false)
		//engine.Log("api/response %v", resp)
		engine.api <- resp
	case "text/event-plain":

		engine.Log("starting to read event")

		copyHeaders(&hdr, resp, true)
		//engine.Log("text/event-plain respons:%v", resp)

		engine.handleEvent(resp)
	case "text/event-json":
		tmp := make(MessageHeader)
		err := json.Unmarshal([]byte(resp.Body), &tmp)
		if err != nil {
			engine.err <- err
			return false
		}
		for k, v := range tmp {
			resp.Header[capitalize(k)] = v
		}
		if v, _ := resp.Header["_body"]; v != "" {
			resp.Body = v
			delete(resp.Header, "_body")
		} else {
			resp.Body = ""
		}
		//engine.Log("text/event-json respons:%v", resp)

		engine.handleEvent(resp)
	case "text/disconnect-notice":
		copyHeaders(&hdr, resp, false)
		engine.Log("text/disconnect-notice respons:%v", resp)

		engine.handleEvent(resp)
	default:
		engine.Log("Unsupported event:", hdr)
	}
	return true
}

func (h *SimpleEngine) RemoteAddr() net.Addr {
	return h.conn.RemoteAddr()
}

func (h *SimpleEngine) Close() error {
	h.Log("closing engine")
	h.close = true
	return h.conn.Close()
}

func (h *SimpleEngine) CreateChannel(channelId string, processor EventProcessor) *Channel {
	channel := &Channel{
		Engine:    h,
		ChannelId: channelId,
		Processor: processor,
	}

	h.Log("starting channel")
	h.chsLock.Lock()
	h.channels[channelId] = channel
	h.chsLock.Unlock()
	return channel
}

func (h *SimpleEngine) handleEvent(ev *Message) error {
	name := ev.Header["Event-Name"]
	h.Log("handleEvent name:%s", name)
	channelId, ok := ev.Header["Unique-Id"]
	var channel *Channel
	var found bool
	h.chsLock.RLock()
	if ok {
		channel, found = h.channels[channelId]
	}
	h.chsLock.RUnlock()

	switch name {
	case "BACKGROUND_JOB":
		//h.Log("---event :%v", ev.Header)
		jobId := ev.Header["Job-Uuid"]
		h.Log("jobId:%s", jobId)
		h.jobsLock.Lock()
		chMsg, ok := h.jobs[jobId]
		if ok {
			h.Log("find job it will be deleted")
			delete(h.jobs, jobId)
			h.Log("notify background job with event")
		} else {
			//event come earlier than command reply
			h.Log("job not found by jobId:%s", jobId)
			chMsg = make(chan *Message)
			h.jobs[jobId] = chMsg
			h.Log("job %s added to queue", jobId)
		}
		h.jobsLock.Unlock()
		go func() {
			chMsg <- ev
		}()
		h.Log("notify the background event back, jobId:%s", jobId)

	case "CHANNEL_CREATE":
		if !found {
			direct := ev.Header["Caller-Direction"]
			callerId := ev.Header["Caller-Caller-ID-Number"]
			did := ev.Header["Caller-Destination-Number"]
			h.Log("call Caller-Direction:%s,Caller-Caller-ID-Number:%s,Caller-Destination-Number:%s", direct, callerId, did)

			processor := h.processorFactory.New(h, ev)
			channel = h.CreateChannel(channelId, processor)
			channel.CallDirection = direct
			channel.State = ACTIVE
			channel.From = callerId
			channel.To = did

			err := processor.Start()
			if err != nil {
				panic(err)
			}
		} else {
			h.Log("Activate the channel:%s because it was found before CHANNEL_CREATE Event", channelId)
			channel.State = ACTIVE
		}

	case "CHANNEL_DESTROY":
		h.Log("destroy channel:%s", channelId)
		h.chsLock.Lock()
		delete(h.channels, channelId)
		h.chsLock.Unlock()

	}
	if found {
		h.Log("send event to channel")
		channel.handleEvent(ev)
	}

	return nil
}

func copyHeaders(src *textproto.MIMEHeader, dst *Message, decode bool) {
	var err error
	for k, v := range *src {
		k = capitalize(k)
		if decode {
			dst.Header[k], err = url.QueryUnescape(v[0])
			if err != nil {
				dst.Header[k] = v[0]
			}
		} else {
			dst.Header[k] = v[0]
		}
	}
}

func capitalize(s string) string {
	if s[0] == '_' {
		return s
	}
	ns := bytes.ToLower([]byte(s))
	if len(s) > 9 && s[1:9] == "variable_" {
		ns[0] = 'V'
		return string(ns)
	}
	toUpper := true
	for n, c := range ns {
		if toUpper {
			if 'a' <= c && c <= 'z' {
				c -= 'a' - 'A'
			}
			ns[n] = c
			toUpper = false
		} else if c == '-' || c == '_' {
			toUpper = true
		}
	}
	return string(s)
}

func (h *SimpleEngine) BGAPI(delay time.Duration, command string, callback func(bool)) error {
	msgCh, err := h.aBGAPI(delay, command)
	if err != nil {
		h.Log("bgapi return error:%v", err)
		return err
	}
	h.Log("bgapi return no error found!")
	go func() {
		select {
		case msg := <-msgCh:
			if msg.Body != "" {
				h.Log("bgapi return body:%s", msg.Body)
				suc := false
				if strings.HasPrefix("+OK", msg.Body) {
					suc = true
				}

				if callback != nil {
					h.Log("bgapi callback:%v", suc)
					callback(suc)
				}

			}

		}
	}()

	return nil
}

func (h *SimpleEngine) aBGAPI(delay time.Duration, command string) (<-chan *Message, error) {
	if strings.IndexAny(command, "\r\n") > 0 {
		return nil, ErrInvalidCommand
	}

	if delay > 0 {
		time.Sleep(delay)
	}
	h.Log("run bgapi %s after %v", command, delay)
	h.writeLock.Lock()
	_, err := fmt.Fprintf(h.conn, "bgapi %s\r\n\r\n", command)
	h.writeLock.Unlock()
	h.Log("run bgapi %s completed", command)
	if err != nil {
		h.Log("error -> %v", err)
		return nil, err
	}

	select {
	case err = <-h.err:
		h.Log("error in bgapi %s reason:%v", command, err)
		return nil, err
	case ev := <-h.cmd:

		//Reply-Text: +OK Job-UUID: c7709e9c-1517-11dc-842a-d3a3942d3d63
		replyTxt := (*ev).Header["Reply-Text"]
		h.Log("cmd %s reply:%s", command, replyTxt)

		//start := len("+OK Job-UUID: ")
		//jobId := replyTxt[start:]
		jobId := ev.Header["Job-Uuid"]

		h.jobsLock.Lock()
		chMsg, ok := h.jobs[jobId]
		if !ok {
			h.Log("add jobId:%s notification channel", jobId)
			chMsg = make(chan *Message)
			h.jobs[jobId] = chMsg
		} else {
			h.Log("found existing jobId:%s notification channel", jobId)
			delete(h.jobs, jobId)
		}
		h.jobsLock.Unlock()
		return chMsg, nil

	}
}

func (h *SimpleEngine) SendMSG(delay time.Duration, uuid string, m MSG, appData string) (bool, error) {

	b := bytes.NewBufferString("sendmsg")
	if uuid != "" {
		if strings.IndexAny(uuid, "\r\n") > 0 {
			return false, ErrInvalidCommand
		}
		b.WriteString(" " + uuid)
	}
	b.WriteString("\n")
	for k, v := range m {
		if strings.IndexAny(k, "\r\n") > 0 {
			return false, ErrInvalidCommand
		}
		if v != "" {
			if strings.IndexAny(v, "\r\n") > 0 {
				return false, ErrInvalidCommand
			}
			b.WriteString(fmt.Sprintf("%s: %s\n", k, v))
		}
	}
	b.WriteString("\n")
	if m["content-length"] != "" && appData != "" {
		b.WriteString(appData)
	}
	h.Log("sendmsg:%s", b.String())
	h.writeLock.Lock()
	_, err := b.WriteTo(h.conn)
	h.writeLock.Unlock()
	if err != nil {
		return false, err
	}
	var (
		ev *Message
	)
	select {
	case err = <-h.err:
		h.Log("eror:%v, sendmsg:%s, ", err, b.String())
		return false, err
	case ev = <-h.cmd:
		h.Log("sendmsg:%s,event:%v", b.String(), ev)
		replyTxt := ev.Header["Reply-Text"]
		if strings.HasPrefix(replyTxt, "+OK") {
			return true, nil
		}
		return false, nil
	}
}

func (h *SimpleEngine) ExecuteUUID(delay time.Duration, uuid, appName, appArg string) (bool, error) {
	return h.SendMSG(delay, uuid, MSG{
		"call-command":     "execute",
		"execute-app-name": appName,
		"execute-app-arg":  appArg,
	}, "")
}

func (r *Message) String() string {
	if r.Body == "" {
		return fmt.Sprintf("%s", r.Header)
	} else {
		return fmt.Sprintf("%s body=%s", r.Header, r.Body)
	}
}

func (r *Message) PrettyPrint() {

	for k := range r.Header {
		fmt.Printf("%s: %#v\n", k, r.Header[k])
	}
	if r.Body != "" {
		fmt.Printf("Body: %#v\n", r.Body)
	}
}
