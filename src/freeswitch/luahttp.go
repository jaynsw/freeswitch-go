package freeswitch

import (
	"encoding/json"
	"github.com/yuin/gopher-lua"
	"io"
	"net"
	"net/http"
	"net/url"
	"time"
)

func httpOpen(L *lua.LState) {
	L.RegisterModule("http", fshttpMethods)
}

var fshttpMethods = map[string]lua.LGFunction{
	"getTable": httpGetTable,
}

func httpGetTable(L *lua.LState) int {
	uuu := L.CheckString(1)
	params := L.OptTable(2, L.NewTable())
	tmout := L.OptInt(3, 10)

	u, err := url.Parse(uuu)
	if err != nil {
		L.ArgError(1, "url is not valid and expected")
	}
	q := u.Query()
	params.ForEach(func(k, v lua.LValue) {
		q.Set(k.String(), v.String())
	})

	u.RawQuery = q.Encode()

	var timeout = time.Duration(tmout) * 1000 * time.Millisecond

	transport := http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.DialTimeout(network, addr, timeout)
		},
	}

	client := http.Client{
		Transport: &transport,
	}

	resp, err := client.Get(u.String())
	if err != nil {
		L.ArgError(1, err.Error())
	}
	defer resp.Body.Close()

	type Message struct {
		Name, Text string
	}
	dec := json.NewDecoder(resp.Body)
	ret := L.NewTable()
	for {
		var m Message
		if err := dec.Decode(&m); err == io.EOF {
			break
		} else if err != nil {
			L.ArgError(1, err.Error())
		}
		ret.RawSet(lua.LString(m.Name), lua.LString(m.Text))
	}

	L.Push(ret)
	return 1
}
