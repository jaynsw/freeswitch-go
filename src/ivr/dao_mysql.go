// dao_mysql.go
package ivr

import (
	"database/sql"
	"github.com/go-gorp/gorp"
	_ "github.com/go-sql-driver/mysql"
)

type DAOMySql struct {
	url string
	dbm *gorp.DbMap
	err error
	txn *gorp.Transaction
}

func openDAO(u string) (DAO, error) {
	db, err := sql.Open("mysql", u)
	if err != nil {
		return nil, err
	}
	dbmap := &gorp.DbMap{
		Db:      db,
		Dialect: gorp.MySQLDialect{"InnoDB", "UTF8"},
	}

	dbmap.AddTableWithName(SipAccount{}, "sip_account").SetKeys(true, "Code")
	dbmap.AddTableWithName(Account{}, "account").SetKeys(true, "Code")
	dbmap.AddTableWithName(DID{}, "did").SetKeys(true, "Code")
	dbmap.AddTableWithName(Call{}, "call").SetKeys(true, "Code")
	dbmap.AddTableWithName(Charge{}, "charge").SetKeys(true, "Code")
	dbmap.AddTableWithName(CallRate{}, "rate").SetKeys(true, "Code")
	dbmap.AddTableWithName(Click{}, "click").SetKeys(true, "Code")

	err = dbmap.CreateTablesIfNotExists()
	if err != nil {
		return nil, err
	}

	return &DAOMySql{
		url: u,
		dbm: dbmap,
		err: nil,
	}, nil
}

func (dao *DAOMySql) Begin() error {
	tx, err := dao.dbm.Begin()
	if err != nil {
		dao.err = err
		return err
	}
	dao.txn = tx
	return nil
}
func (dao *DAOMySql) Commit() error {
	if dao.err != nil {
		return dao.err
	}
	return dao.txn.Commit()
}
func (dao *DAOMySql) Rollback() error {
	if dao.err != nil {
		return dao.err
	}
	return dao.txn.Rollback()
}

func (dao *DAOMySql) SaveSipAccount(account *SipAccount) error {
	return dao.dbm.Insert(account)
}

func (dao *DAOMySql) GetSipAccountByNameAndHost(name, host string) (*SipAccount, error) {
	account := SipAccount{}
	err := dao.dbm.SelectOne(&account, "select * from sip_account where host=? and name=?", host, name)
	if err != nil {
		return nil, err
	}
	return &account, err
}
func (dao *DAOMySql) ListWebisteSipAccountByAccountCode(accountcode int64, website string) ([]SipAccount, error) {
	var accounts []SipAccount
	_, err := dao.dbm.Select(&accounts, "select * from sip_account where account_code=? and website=?", accountcode, website)
	if err != nil {
		return nil, err
	}
	return accounts, err
}

func (dao *DAOMySql) ListWebsiteSipAccount(website string, offset int64, len int) ([]SipAccount, error) {
	var accounts []SipAccount
	_, err := dao.dbm.Select(&accounts, "select * from sip_account where website=? limit ?,？", website, offset, len)
	if err != nil {
		return nil, err
	}
	return accounts, err
}

func (dao *DAOMySql) CountWebsiteSipAccount(website string) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from sip_account where website=?", website)
}

func (dao *DAOMySql) ListSipAccount(offset int64, len int) ([]SipAccount, error) {
	var accounts []SipAccount
	_, err := dao.dbm.Select(&accounts, "select * from sip_account limit ?,？", offset, len)
	if err != nil {
		return nil, err
	}
	return accounts, err
}

func (dao *DAOMySql) CountSipAccount() (int64, error) {
	return dao.dbm.SelectInt("select count(*) from sip_account")
}

func (dao *DAOMySql) DeleteSipAccountByCode(sipaccountcode int64) error {
	_, err := dao.dbm.Exec("delete from sip_account where code=? ", sipaccountcode)
	return err
}

////////////

func (dao *DAOMySql) SaveAccount(account *Account) error {
	return dao.dbm.Insert(account)
}

func (dao *DAOMySql) GetAccountByCode(code int64) (*Account, error) {
	account := Account{}
	err := dao.dbm.SelectOne(&account, "select * from account where code=? ", code)
	if err != nil {
		return nil, err
	}
	return &account, err
}

func (dao *DAOMySql) GetWebsiteAccountByName(name, website string) (*Account, error) {
	account := Account{}
	err := dao.dbm.SelectOne(&account, "select * from account where website=? and name=?", website, name)
	if err != nil {
		return nil, err
	}
	return &account, err
}

func (dao *DAOMySql) ListWebsiteAccount(website string, offset int64, len int) ([]Account, error) {
	var accounts []Account
	_, err := dao.dbm.Select(&accounts, "select * from account where website=? limit ?,？", website, offset, len)
	if err != nil {
		return nil, err
	}
	return accounts, err
}

func (dao *DAOMySql) CountWebsiteAccount(website string) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from account where website=?", website)
}

func (dao *DAOMySql) ListAccount(offset int64, len int) ([]Account, error) {
	var accounts []Account
	_, err := dao.dbm.Select(&accounts, "select * from account limit ?,？", offset, len)
	if err != nil {
		return nil, err
	}
	return accounts, err
}

func (dao *DAOMySql) CountAccount() (int64, error) {
	return dao.dbm.SelectInt("select count(*) from account")
}

func (dao *DAOMySql) DeleteAccountByCode(sipaccountcode int64) error {
	_, err := dao.dbm.Exec("delete from account where code=? ", sipaccountcode)
	return err
}

func (dao *DAOMySql) SaveDID(did *DID) error {
	return dao.dbm.Insert(did)
}

func (dao *DAOMySql) GetDIDByCode(didcode int64) (*DID, error) {
	did := DID{}
	err := dao.dbm.SelectOne(&did, "select * from did where code=?", didcode)
	if err != nil {
		return nil, err
	}
	return &did, err
}

func (dao *DAOMySql) GetDIDByExtension(ext string) (*DID, error) {
	did := DID{}
	err := dao.dbm.SelectOne(&did, "select * from did where ext=?", ext)
	if err != nil {
		return nil, err
	}
	return &did, err
}

func (dao *DAOMySql) ListDIDByAccountCode(accountcode int64) ([]DID, error) {
	var dids []DID
	_, err := dao.dbm.Select(&dids, "select * from did where account_code=? ", accountcode)
	if err != nil {
		return nil, err
	}
	return dids, err
}

func (dao *DAOMySql) ListWebsiteDID(website string, offset int64, len int) ([]DID, error) {
	var dids []DID
	_, err := dao.dbm.Select(&dids, "select * from did where website=? limit ?,? ", website, offset, len)
	if err != nil {
		return nil, err
	}
	return dids, err
}

func (dao *DAOMySql) CountWebsiteDID(website string) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from did where website=?", website)
}

func (dao *DAOMySql) ListDID(offset int64, len int) ([]DID, error) {
	var dids []DID
	_, err := dao.dbm.Select(&dids, "select * from did ")
	if err != nil {
		return nil, err
	}
	return dids, err
}

func (dao *DAOMySql) CountDID() (int64, error) {
	return dao.dbm.SelectInt("select count(*) from did ")
}

func (dao *DAOMySql) DeleteDIDByCode(didcode int64) error {
	_, err := dao.dbm.Exec("delete from did where code=? ", didcode)
	return err
}

func (dao *DAOMySql) SaveCall(call *Call) error {
	return dao.dbm.Insert(call)
}

func (dao *DAOMySql) GetCallByCode(callcode int64) (*Call, error) {
	call := Call{}
	err := dao.dbm.SelectOne(&call, "select * from call where code=?", callcode)
	if err != nil {
		return nil, err
	}
	return &call, err
}

func (dao *DAOMySql) ListCallBySessionCode(session int64) ([]Call, error) {
	var calls []Call
	_, err := dao.dbm.Select(&calls, "select * from call where session=?", session)
	if err != nil {
		return nil, err
	}
	return calls, err
}

func (dao *DAOMySql) ListCallByExtension(ext string, start, end int64, offset int64, len int) ([]Call, error) {
	var calls []Call
	_, err := dao.dbm.Select(&calls, "select * from call where ext=? and time_from >= ? and time_from < ? limit ?,?", ext, start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return calls, err
}

func (dao *DAOMySql) CountCallByExtension(ext string, start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from call where ext=? and time_from >= ? and time_from < ?", ext, start, end)
}

func (dao *DAOMySql) ListWebsiteCall(website string, start, end int64, offset int64, len int) ([]Call, error) {
	var calls []Call
	_, err := dao.dbm.Select(&calls, "select * from call where website=? and time_from >=s ? and time_from < ? limit ?,?", website, start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return calls, err
}

func (dao *DAOMySql) CountWebsiteCall(website string, start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from call where website=? and time_from >= ? and time_from < ?", website, start, end)
}

func (dao *DAOMySql) ListCall(start, end int64, offset int64, len int) ([]Call, error) {
	var calls []Call
	_, err := dao.dbm.Select(&calls, "select * from call where time_from >=s ? and time_from < ? limit ?,?", start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return calls, err
}

func (dao *DAOMySql) CountCall(start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from call where time_from >= ? and time_from < ?", start, end)
}

func (dao *DAOMySql) SaveClick(click *Click) error {
	return dao.dbm.Insert(click)
}

func (dao *DAOMySql) GetClickByCode(clickcode int64) (*Click, error) {
	click := &Click{}
	err := dao.dbm.SelectOne(click, "select * from click where code=?", clickcode)
	if err != nil {
		return nil, err
	}
	return click, err
}

func (dao *DAOMySql) ListClickByExtension(ext string, start, end int64, offset int64, len int) ([]Click, error) {
	var clicks []Click
	_, err := dao.dbm.Select(&clicks, "select * from click where ext=? and time_from >= ? and time_from < ? limit ?,?", ext, start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return clicks, err
}

func (dao *DAOMySql) CountClickByExtension(ext string, start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from click where ext=? and time_from >= ? and time_from < ?", ext, start, end)
}

func (dao *DAOMySql) ListWebsiteClick(website string, start, end int64, offset int64, len int) ([]Click, error) {
	var clicks []Click
	_, err := dao.dbm.Select(&clicks, "select * from click where website=? and time_from >=s ? and time_from < ? limit ?,?", website, start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return clicks, err
}

func (dao *DAOMySql) CountWebsiteClick(website string, start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from click where website=? and time_from >= ? and time_from < ?", website, start, end)
}

func (dao *DAOMySql) ListClick(start, end int64, offset int64, len int) ([]Click, error) {
	var clicks []Click
	_, err := dao.dbm.Select(&clicks, "select * from click where time_from >=s ? and time_from < ? limit ?,?", start, end, offset, len)
	if err != nil {
		return nil, err
	}
	return clicks, err
}

func (dao *DAOMySql) CountClick(start, end int64) (int64, error) {
	return dao.dbm.SelectInt("select count(*) from click where time_from >= ? and time_from < ?", start, end)
}
