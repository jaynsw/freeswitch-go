// ivr_handler
package ivr

import (
	"bytes"
	"fmt"
	fs "freeswitch"
	"strings"
	"uuid"
)

type sipPhoneHandler struct {
	ivr             *IVR
	outboundChannel *fs.Channel
	outboundPlayer  *fs.Player
	status          string
	stage           string
	numbers         []string
	next            int
}

func newSipPhonePipeline(h *IVR) fs.EventHandler {
	return &sipPhoneHandler{
		ivr: h,
	}
}

func (h *sipPhoneHandler) handleEventOutboundCall(ch *fs.Channel, ev *fs.Message) error {
	eventName := ev.Header["Event-Name"]

	if "CHANNEL_HANGUP" == eventName {
		if ch == h.outboundChannel && "bridged" != h.stage {
			cause, ok := ev.Header["Hangup-Cause"]
			if ok {
				h.ivr.engine.Log("outboundCallHandler hangup cause:%s", cause)

			}
			return h.dialNext()
		}
	}
	if "CHANNEL_ANSWER" == eventName {
		h.outboundPlayer = &fs.Player{Engine: h.ivr.engine, Channel: h.outboundChannel}

		return h.outboundPlayer.Bridge(0, h.ivr.channel.ChannelId, func(ok bool) {
			if ok {
				h.stage = "bridged"
			}
		})

	}

	return nil
}

func (h *sipPhoneHandler) outbound(nums string) error {

	h.status = "outbound"

	channelId := uuid.New()
	direction := "outbound"
	callerId := h.ivr.channel.From

	h.outboundChannel = h.ivr.engine.CreateChannel(channelId)
	h.outboundChannel.CallDirection = direction
	h.outboundChannel.State = fs.INIT
	h.outboundChannel.From = callerId

	pipeline := h.ivr.channel.Pipeline
	h.outboundChannel.Pipeline = pipeline

	h.numbers = strings.Split(nums, ",")

	h.dialNext()
	return nil
}

func (h *sipPhoneHandler) dialNext() error {
	if h.next >= len(h.numbers) {
		nextNumber := h.numbers[h.next]
		h.next += 1
		return h.dial(nextNumber)
	} else {
		h.ivr.HandleNotFound()
		return nil
	}
}

func (h *sipPhoneHandler) dial(num string) error {
	ivr := h.ivr
	eng := ivr.engine
	sofia := bytes.Buffer{}
	sipNumberPrefix := ivr.GetSipNumberPrefix()
	gateway := h.ivr.account.Gateway
	did := ivr.did
	timeout := 100
	callerId := ivr.channel.From
	if strings.HasPrefix(num, sipNumberPrefix) {
		sofia.WriteString("user/" + num + "@" + did.Website)
	} else {
		sofia.WriteString("sofia/gateway/" + gateway + "/" + num)
	}
	data := fmt.Sprintf("originate {origination_uuid=%s ,return_ring_ready=true,fail_on_single_reject=true,ignore_early_media=true,origination_caller_id_number=%s } %s &park() %d", h.outboundChannel.ChannelId, callerId, sofia, timeout)
	return eng.BGAPI(0, data, func(ok bool) {
		if !ok {
			h.dialNext()
		}
	})

}

func (h *sipPhoneHandler) Start() error {
	engine := h.ivr.engine
	engine.Log("answer the inbound call.")
	return h.ivr.player.Answer(0, func(ok bool) {
		engine.Log("create outbound call %s", h.ivr.channel.To)
		h.outbound(h.ivr.channel.To)
	})

}

func (h *sipPhoneHandler) HandleEvent(ch *fs.Channel, ev *fs.Message) error {

	if "outbound" == h.status {
		return h.handleEventOutboundCall(ch, ev)
	}
	return nil
}
