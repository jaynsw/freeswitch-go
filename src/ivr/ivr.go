package ivr

import (
	fs "freeswitch"
	"strings"
)

type IVR struct {
	event         chan *fs.ChannelMessage
	Close         bool
	engine        fs.Engine
	properties    map[string]string
	channel       *fs.Channel
	originMessage *fs.Message
	eventHandler  fs.EventHandler
	did           *DID
	player        *fs.Player
	dao           DAO
	account       *Account
}

func (h *IVR) GetWav(f string) string {
	root := h.properties["wav_root"]
	return root + "/" + f
}

func (h *IVR) GetSipNumberPrefix() string {
	return h.properties["sip_number_prefix"]
}

func (h *IVR) GetDBURL() string {
	return h.properties["db_url"]
}

func (h *IVR) GetPublicClickNumber() string {
	return h.properties["public_click_number"]
}

func (h *IVR) GetFormNumberPrefix() string {
	return h.properties["form_number_prefix"]
}

/*
func (h *IVR) GetPhoneNumberPrefix() string {
	return h.properties["phone_number_prefix"]
}
*/

type EndPointType int

const (
	SIP_EXT EndPointType = iota
	FORM_EXT
	PSTN_EXT
	RSIP_EXT
)

type EndPoint struct {
	Extension string
	EPType    EndPointType
	Host      string
	Value     string
}

func (h *IVR) ParseNumber(val string) *EndPoint {
	sipPrefix := h.GetSipNumberPrefix()
	formPrefix := h.GetFormNumberPrefix()
	endpointType := PSTN_EXT
	extension := val
	var host string
	if strings.HasPrefix(val, sipPrefix) {
		endpointType = SIP_EXT
		extension = val[len(sipPrefix):]
	} else if strings.HasPrefix(val, formPrefix) {
		endpointType = FORM_EXT
		extension = val[len(formPrefix):]
	} else if idx := strings.Index(val, "@"); idx > 0 {
		endpointType = RSIP_EXT
		extension = val[0:idx]
		host = val[idx:]
	} else {
		endpointType = PSTN_EXT
		extension = val
	}

	ep := &EndPoint{
		Extension: extension,
		EPType:    endpointType,
		Host:      host,
		Value:     val,
	}
	return ep

}

func (h *IVR) init() {
	/*
		from := h.channel.From
		to := h.channel.To
		accountCode, accoundCodeFound := h.originMessage.Header["account_code"]
	*/
	h.event = make(chan *fs.ChannelMessage)
	h.eventHandler = newSipPhonePipeline(h)

	mysqlURL := h.GetDBURL()
	dao, err := openDAO(mysqlURL)
	if err != nil {
		panic(err)
	}
	h.dao = dao

	ch := h.channel

	direction := ch.CallDirection
	ext := ch.To
	if "inbound" == direction {
		ext = ch.To
	} else {
		ext = ch.From
	}

	h.did, err = dao.GetDIDByExtension(ext)
	if err != nil {
		panic(err)
	}

	h.account, err = dao.GetAccountByCode(h.did.AccountCode)
	if err != nil {
		panic(err)
	}

}

func (h *IVR) Start() error {
	engine := h.engine
	engine.Log("pipeline started")

	go func() {
		for {
			engine.Log("channel %s loop", h.channel.ChannelId)
			if h.Close {
				break
			}
			select {

			case ev := <-h.event:
				ch := ev.Channel
				msg := ev.Message
				name := msg.Header["Event-Name"]
				engine.Log("channel object %s received event %s \n%v", ch.ChannelId, name, msg)

				h.eventHandler.HandleEvent(ch, msg)

			}
		}
	}()

	return h.eventHandler.Start()
}

func (h *IVR) GetChannel() chan<- *fs.ChannelMessage {
	return h.event
}

func (h *IVR) HandleNotFound() {
	h.player.Hangup(0, "404", nil)
}

func (h *IVR) HandleNoInput() {
	h.player.Hangup(0, "404", nil)
}

func (h *IVR) HandleNoMatch() {
	h.player.Hangup(0, "404", nil)
}

type IVRFactory struct {
	Properties map[string]string
}

func (h IVRFactory) New(eng fs.Engine, ch *fs.Channel, msg *fs.Message) fs.Pipeline {

	pipeline := &IVR{
		engine:        eng,
		properties:    h.Properties,
		channel:       ch,
		originMessage: msg,
		player:        &fs.Player{Engine: eng, Channel: ch},
	}
	pipeline.init()

	return pipeline
}
