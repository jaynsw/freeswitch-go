// ivr.go
package ivr

import ()

type DIDType int

const (
	SIP_DID DIDType = iota
	IVR_DID
	PSTN_DID
)

type SipAccount struct {
	Code        int64  `db:"code"`
	Name        string `db:"name"`
	Secret      string `db:"scret"`
	Host        string `db:"host"`
	Port        string `db:"port"`
	Website     string `db:"website"`
	AccountCode int64  `db:"account_code"`
	RegIP       string `db:"reg_ip"`
	UserAgent   string `db:"user_agent"`
	RegTime     int64  `db:"reg_time"`
}

type Account struct {
	Code      int64  `db:"code"`
	Name      string `db:"name"`
	Secret    string `db:"scret"`
	Website   string `db:"website"`
	Gateway   string `db:"gateway"`
	LoginIP   string `db:"login_ip"`
	UserAgent string `db:"user_agent"`
	LoginTime int64  `db:"login_time"`
	Currency  string `db:"currency"`
}

type DID struct {
	Code        int64   `db:"code"`
	Extension   string  `db:"ext"`
	DIDType     DIDType `db:"eptype"`
	Provider    string  `db:"provider"`
	AccountCode int64   `db:"account_code"`
	Website     string  `db:"website"`
	Email       string  `db:"email"`
	Timezone    string  `db:"timezone"`
}

type Call struct {
	Code      int64  `db:"code"`
	From      string `db:"call_from"`
	To        string `db:"call_to"`
	Session   string `db:"session"`
	TimeFrom  int64  `db:"time_from"`
	TimeTo    int64  `db:"time_to"`
	Status    string `db:"status"`
	Website   string `db:"website"`
	Ext       int64  `db:"ext"`
	Direction string `db:"direction"`
}

type Charge struct {
	Code         int64  `db:"code"`
	Amount       int64  `db:"amount"`
	Category     string `db:"category"`
	Comment      string `db:"comment"`
	CreationTime int64  `db:"creation_time"`
	AccountCode  int64  `db:"account_code"`
	Transaction  string `db:"txn"`
	Website      string `db:"website"`
	CallCode     int64  `db:"call_code"`
}

type CallRate struct {
	Code       int64  `db:"code"`
	Area       string `db:"area"`
	Country    string `db:"country"`
	Name       string `db:"name"`
	Amount     int64  `db:"amount"`
	Plan       string `db:"plan"`
	ChargeType string `db:"charge_type"`
}

type Click struct {
	Code         int64  `db:"code"`
	Task         string `db:"task"`
	TaskCode     string `db:"task_code"`
	ＴaskData     string `db:"task_data"`
	CallCode     string `db:"call_code"`
	ServiceCode  string `db:"service_code"`
	Website      string `db:"website"`
	CreationTime int64  `db:"creation_time"`
	Referer      string `db:"referer"`
	IP           string `db:"ip"`
	UserAgent    string `db:"user_agent"`
	SearchEngine string `db:"search_engine"`
	Keywords     string `db:"keywords"`
	Message      string `db:"msg"`
}

type DAO interface {
	Begin() error
	Commit() error
	Rollback() error

	SaveAccount(account *Account) error
	GetAccountByCode(accountcode int64) (*Account, error)
	GetWebsiteAccountByName(name, website string) (*Account, error)
	ListWebsiteAccount(website string, offset int64, len int) ([]Account, error)
	CountWebsiteAccount(website string) (int64, error)
	ListAccount(offset int64, len int) ([]Account, error)
	CountAccount() (int64, error)
	DeleteAccountByCode(accountcode int64) error

	SaveSipAccount(account *SipAccount) error
	GetSipAccountByNameAndHost(name, host string) (*SipAccount, error)
	ListWebisteSipAccountByAccountCode(accountcode int64, website string) ([]SipAccount, error)
	ListWebsiteSipAccount(website string, offset int64, len int) ([]SipAccount, error)
	CountWebsiteSipAccount(website string) (int64, error)
	ListSipAccount(offset int64, len int) ([]SipAccount, error)
	CountSipAccount() (int64, error)
	DeleteSipAccountByCode(sipaccountcode int64) error

	SaveDID(ep *DID) error
	GetDIDByCode(epcode int64) (*DID, error)
	GetDIDByExtension(ext string) (*DID, error)
	ListDIDByAccountCode(accountcode int64) ([]DID, error)
	ListWebsiteDID(website string, offset int64, len int) ([]DID, error)
	CountWebsiteDID(website string) (int64, error)
	ListDID(offset int64, len int) ([]DID, error)
	CountDID() (int64, error)
	DeleteDIDByCode(didcode int64) error

	SaveCall(call *Call) error
	GetCallByCode(callcode int64) (*Call, error)
	ListCallBySessionCode(sessioncode int64) ([]Call, error)
	ListCallByExtension(ext string, start, end int64, offset int64, len int) ([]Call, error)
	CountCallByExtension(ext string, start, end int64) (int64, error)
	ListWebsiteCall(website string, start, end int64, offset int64, len int) ([]Call, error)
	CountWebsiteCall(website string, start, end int64) (int64, error)
	ListCall(start, end int64, offset int64, len int) ([]Call, error)
	CountCall(start, end int64) (int64, error)

	SaveClick(click *Click) error
	GetClickByCode(clickcode int64) (*Click, error)
	ListClickByExtension(ext string, start, end int64, offset int64, len int) ([]Click, error)
	CountClickByExtension(exct string, start, end int64) (int64, error)
	ListWebsiteClick(website string, start, end int64, offset int64, len int) ([]Click, error)
	CountWebsiteClick(website string, start, end int64) (int64, error)
	ListClick(start, end int64, offset int64, len int) ([]Click, error)
	CountClick(start, end int64) (int64, error)
}
