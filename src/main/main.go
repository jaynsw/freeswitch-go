package main

import (
	"flag"
	"fmt"
	fs "freeswitch"
	props "github.com/dmotylev/goproperties"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
	"time"
	"uuid"
)

var config = flag.String("config", "", "webpbx.properties") // Q=17, R=18

var fsock fs.Engine

func main() {
	flag.Parse()

	if *config == "" {
		path, err := os.Getwd()
		if err != nil {
			log.Printf("error:%v", err)
			return
		}
		*config = path + "/webpbx.properties"
	}
	log.Printf("config:%s", *config)
	p, _ := props.Load(*config)
	Run(p)
}

func Run(props map[string]string) {
	fsock = startIVR(props)
	flag.Parse()
	r := mux.NewRouter()
	ivr := WebIVR{
		fsock,
	}
	r.HandleFunc("/call/{profile}/{ext}", ivr.ClickCallHandler)
	http.Handle("/", r)
	addr := props["web_addr"]

	err := http.ListenAndServe(addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}

}

type WebIVR struct {
	Engine fs.Engine
}

func (ivr WebIVR) ClickCallHandler(w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	profile, ok := vars["profile"]
	if !ok {
		fmt.Errorf("the parameter profile is not found!")
	}
	ext, ok := vars["ext"]
	if !ok {
		fmt.Errorf("the parameter ext is not found!")
	}
	opt := make(map[string]string)
	opt["origination_uuid"] = uuid.New()
	flag := false
	data := ""
	for k, v := range opt {
		if !flag {
			data += "{" + k + ":" + v
			flag = true
		} else {
			data += "," + k + ":" + v
		}
	}
	if flag {
		data += "}"
	}

	cmd := "originate " + data + "sofia/" + profile + "/" + ext + " &park()"
	ivr.Engine.BGAPI(time.Duration(0), cmd, nil)
}

func startIVR(props map[string]string) fs.Engine {

	factory := fs.NewFactory(props)
	addr, ok := props["fs_addr"]
	if !ok {
		log.Printf("freeswitch eventsocket address not found!")
		return nil
	}
	pwd, ok := props["fs_pwd"]
	if !ok {
		log.Printf("freeswitch eventsocket password not found!")
		return nil
	}

	engine, err := fs.Dial(addr, pwd, factory)
	if err != nil {
		log.Printf("Engine Dial error:%v", err)
		return nil
	}
	return engine
}
