
local uuid = require "uuid"
local fs = require "freeswitch"
local _G = _G

module(...)

Player = {}

function Player:new(chid)
	o = {channelid=chid}
	_G.setmetatable(o, self)
	self.__index = self
	return o
end

function Player:uuidCommand(delay, cmd, args)
	
	local data = self.channelid
	_G.print("----xxx--->" .. data)
	if args then
		data = data .." " .. args
	end
	fs.bgapi(delay, cmd .. " " .. data)
end

function Player:hangup(cause, delay)
	local delay = delay or 0
	if not cause then
		self:uuidCommand(delay, "uuid_kill")
	else 
		self:uuidCommand(delay, "uuid_kill", cause)
	end
end

function Player:answer(delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_answer")
end


function Player:playback(path, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_broadcast", "playback::" .. path)
end


function Player:breakk(delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_break", "all")
end



function Player:senddtmf(dtmf, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_send_dtmf", dtmf)
end

function Player:fileexists(path, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "file_exists", path)
end

function Player:bridge(chid, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_bridge", chid)
end

function Player:recordstart(path, maxduration, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_record", "start " .. path .. " " .. maxduration)
end

function Player:recordstop(path, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_record", "stop " .. path)
end

function Player:recordmask(path, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_record", "mask " .. path)
end

function Player:recordunmask(path, delay)
	local delay = delay or 0
	self:uuidCommand(delay, "uuid_record", "umask " .. path)
end

function Player:originate(optable, url, delay)
	local delay = delay or 0
	chid = uuid.new()
	fs.createchannel(chid)
	
	optable["origination_uuid"] = chid
	
	flag = false
	opt = ""
	for k,v in pairs(optable) do
		if not flag then
			opt = opt .. " {" .. key .. ":" .. v
			flag = true
		else
			opt = opt .. "," .. k .. ":" .. v
		end	
		
	end
	if flag then
		opt = opt .. "}"
	end
	cmd = "originate" .. opt .." " .. url .. " &park()"
	fs.bgapi(delay, cmd)
end

