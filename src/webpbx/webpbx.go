package webpbx

import (
	"flag"
	fs "freeswitch"
	"github.com/gorilla/mux"
	"ivr"
	"log"
	"net/http"
)

var fsock fs.Engine

func Run(props map[string]string) {
	fsock = startIVR(props)
	flag.Parse()
	r := mux.NewRouter()
	http.Handle("/", r)
	addr := props["web_addr"]

	err := http.ListenAndServe(addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}

func startIVR(props map[string]string) fs.Engine {

	factory := ivr.IVRFactory{
		Properties: props,
	}
	addr, ok := props["fs_addr"]
	if !ok {
		log.Printf("freeswitch eventsocket address not found!")
		return nil
	}
	pwd, ok := props["fs_pwd"]
	if !ok {
		log.Printf("freeswitch eventsocket password not found!")
		return nil
	}

	engine, err := fs.Dial(addr, pwd, factory)
	if err != nil {
		log.Printf("Engine Dial error:%v", err)
		return nil
	}
	return engine
}
